# README #

Allows to filter the csv data file with only given columns and only given records/lines.

See details on [http://alexapps.net/filter-csv-files/](http://alexapps.net/filter-csv-files/)

### Details ###

* Data file

```
#!bash

$ cat data.csv 
instrument      country price
IBM     US      199.12
MSFT    US      26.22
VOD-LON GB      44.15
```

* How to run script

```
#!bash

$ cat run_test.sh 
#!/bin/bash

./filter_csv.py -c instrument -c price -r country=US data.csv
```

* Output

```
#!bash

$ ./run_test.sh 
instrument      price
IBM     199.12
MSFT    26.22
```

* Usage

```
#!bash
$ ./filter_csv.py -h
Filters csv file with specified list of columns and record filters.

Usage: ./filter_csv.py [OPTIONS] [data_file]

  where
    [OPTIIONS]:

      -h, --help
          This help is printed.
      -s, --separator
          Column separator, by default it is \t.

      -r <rule>, --record=<rule>
          Rule in form <column>=<regex> to match the value. It can be specified several times.
          Example: -r instrument=AA.* -r country=US

      -c <column_regex>, --column=<column_regex>
          Column to include in output. It can be specified several times.
          Example: -c instr.* -c country

    [data_file]:
      If not specified, then stdin is used to allow piping.

Example: ./filter_csv.py -c instrument -c country -r country=^US$ data.csv
```