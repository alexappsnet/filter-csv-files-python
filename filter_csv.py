#!/usr/bin/env python

import csv
from functools import partial
from pstats import func_strip_path
import re
import time
import getopt
import sys


class Options():
    pass


options = Options

options.separator = '\t'
options.records = []
options.columns = []


def usage(text = None):
    if text:
        print('Error:', text)

    print("""\
Filters csv file with specified list of columns and record filters.

Usage: {0} [OPTIONS] [data_file]

  where
    [OPTIIONS]:

      -h, --help
          This help is printed.
      -s, --separator
          Column separator, by default it is \\t.

      -r <rule>, --record=<rule>
          Rule in form <column>=<regex> to match the value. It can be specified several times.
          Example: -r instrument=AA.* -r country=US

      -c <column_regex>, --column=<column_regex>
          Column to include in output. It can be specified several times.
          Example: -c instr.* -c country

    [data_file]:
      If not specified, then stdin is used to allow piping.

Example: {0} -c instrument -c country -r country=^US$ data.csv
""".format(sys.argv[0]))
    sys.exit(1)


try:
    opts, args = getopt.getopt(sys.argv[1:], 'hs:r:c:', ['help', 'separator=', 'record=', 'column='])
except getopt.GetoptError as e:
    usage()
    sys.exit(1)

for opt, arg in opts:
    if opt in ("-h", "--help"):
        usage()
    elif opt in ("-s", "--separator"):
        options.separator = arg
    elif opt in ("-r", "--record"):
        options.records.append(arg)
    elif opt in ("-c", "--column"):
        options.columns.append(arg)

options.args = args

# print('Separator:     |%s|' % options.separator)
# print('Record Filter: |%s|' % ', '.join(options.records))
# print('Column Filter: |%s|' % ', '.join(options.columns))
# print('Args: |%s|' % ', '.join(options.args))


class CustomDialect(csv.excel):
    delimiter = options.separator


csv.register_dialect('custom_dialect', CustomDialect)


def dump(csv_data, separator, record_filters, columns):

    for row in csv_data:
        for record_filter in record_filters:
            if not record_filter(row):
                break
        else:
            line = separator.join([row[col] for col in columns])
            print(line)


filename = sys.argv[-1]

try:
    f = sys.stdin
    if len(options.args):
        f = open(filename)

    csv_data = csv.DictReader(f, dialect='custom_dialect')

    # determine list of columns for output
    columns = []
    if len(options.columns):
        for pattern in options.columns:
            pattern = re.compile(pattern, re.IGNORECASE)
            for column in ([x for x in csv_data.fieldnames if re.search(pattern, x)]):
                if column not in columns:
                    columns.append(column)
    else:
        columns = list(csv_data.fieldnames)

    print(options.separator.join(columns))

    # determine list of filter functions for records
    record_filters = []
    for record_filter in options.records:
        tokens = record_filter.split('=')
        if len(tokens) != 2:
            usage('-r should be in form column=regex')
        column = tokens[0]
        regex = re.compile(tokens[1], re.IGNORECASE)

        def _filter(column, regex, row):
            return regex.search(row[column])

        record_filters.append(partial(_filter, column, regex))

    dump(csv_data, options.separator, record_filters, columns)
finally:
    f.close()
